const app = module.exports = require('express')();
const bodyParser = require('body-parser');
const proxy = require('http-proxy-middleware');
const btoa = require('btoa');

const options = {
  target: 'http://172.16.160.7:9200/',
  changeOrigin: true,
  onProxyReq: (proxyReq, req) => {
      // proxyReq.setHeader(
      //     'Authorization',
      //     `Basic ${btoa('cf7QByt5e:d2d60548-82a9-43cc-8b40-93cbbe75c34c')}`
      // );
      /* transform the req body back from text */
      const { body } = req;
      if (body) {
          if (typeof body === 'object') {
              proxyReq.write(JSON.stringify(body));
          } else {
              proxyReq.write(body);
          }
      }
  }
}

app.get('/', (req, res) => {
  res.send({msg: 'hello! Server is up and running'});
});

app.use('/elastic', require('./elastic'));

app.use(bodyParser.text({ type: 'application/x-ndjson' }));

app.use('*', proxy(options));

// the catch all route
// app.all('*', (req, res) => {
//   res.status(404).send({msg: 'not found'});
// });

