function generateMappingSettings() {
    autosuggestIndex = {
        settings: {
            index: {
            analysis: {
                filter: {
                permutations: {
                    max_shingle_size: "5",
                    min_shingle_size: "2",
                    type: "shingle"
                },
                autocomplete_filter: {
                    type: "edge_ngram",
                    min_gram: "1",
                    max_gram: "20"
                },
                delimiter: {
                    catenate_all: "true",
                    type: "word_delimiter",
                    catenate_numbers: "true",
                    preserve_original: "true",
                    catenate_words: "true"
                }
                },
                analyzer: {
                autocomplete: {
                    filter: [
                    "lowercase",
                    "autocomplete_filter",
                    "delimiter",
                    "permutations"
                    ],
                    type: "custom",
                    tokenizer: "standard"
                },
                my_english: {
                    type: "standard",
                    stopwords: "_english_"
                }
                },
                normalizer: {
                    my_normalizer: {
                      type: "custom",
                      char_filter: [],
                      filter: ["lowercase", "asciifolding"]
                    }
                }

            }
            }
        },
        mappings: {
            "event": {
            properties: {
                artists: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword"
                    }
                }
                },
                cast: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword"
                    }
                }
                },
                category: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword"
                    }
                }
                },
                contentCategory: {
                    type: "keyword",
                    store: true,
                    normalizer: "my_normalizer"
                },
                content_id: {
                "type": "long"
                },
                coverImage: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                    }
                }
                },
                crew: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                    }
                }
                },
                duration: {
                "type": "long"
                },
                endDateEpoch: {
                "type": "long"
                },
                endTime: {
                "type": "date"
                },
                event_details_id: {
                "type": "long"
                },
                followCount: {
                "type": "long"
                },
                formattedLaunchDate: {
                type: "text",
                "fields": {
                    "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                    }
                }
                },
                generic_details_id: {
                "type": "long"
                },
                genre: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                    }
                }
                },
                globalLaunch: {
                "type": "boolean"
                },
                images: {
                properties: {
                    "height": {
                    "type": "long"
                    },
                    "url": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    },
                    "width": {
                    "type": "long"
                    }
                }
                },
                keywords: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                    }
                }
                },
                languages: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                    }
                }
                },
                launchCountries: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                    }
                }
                },
                launchDate: {
                "type": "date"
                },
                launchDateEpoch: {
                "type": "long"
                },
                links: {
                properties: {
                    name: {
                    type: "text",
                    fields: {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    },
                    "url": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    }
                }
                },
                movie_details_id: {
                "type": "long"
                },
                name: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                    }
                }
                },
                objectID: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                    }
                }
                },
                popularity: {
                "type": "long"
                },
                posterImage: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                    }
                }
                },
                seo: {
                properties: {
                    anchorText: {
                    type: "text",
                    fields: {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    },
                    metaDescription: {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    },
                    pageTitle: {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    },
                    robotsMetaTag: {
                    "type": "boolean"
                    }
                }
                },
                shortDesc: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                    }
                }
                },
                slug: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                    }
                }
                },
                songVideos: {
                properties: {
                    youtube_id: {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    },
                    youtube_title: {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    }
                }
                },
                startTime: {
                "type": "date"
                },
                subCategory: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                    }
                }
                },
                tickets: {
                properties: {
                    bookingLink: {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    },
                    endTime: {
                    "type": "date"
                    },
                    maxPrice: {
                    "type": "long"
                    },
                    minPrice: {
                    "type": "long"
                    },
                    name: {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    },
                    startTime: {
                    "type": "date"
                    }
                }
                },
                trailerUrl: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                    }
                }
                },
                venue: {
                properties: {
                    address: {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    },
                    city: {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    },
                    country: {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    },
                    gmapLink: {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    },
                    name: {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    },
                    state: {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    }
                }
                },
                videoYoutubeId: {
                "type": "text",
                "fields": {
                    "keyword": {
                    "type": "keyword",
                    "ignore_above": 256
                    }
                }
                },
                videos: {
                "properties": {
                    "youtube_id": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    },
                    "youtube_title": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                        }
                    }
                    }
                }
                }
            }
            }
        }
    };
    return autosuggestIndex;
  }
  
  module.exports = {
    AUTOSUGGEST_INDEX_SETTINGS_MAPPING: generateMappingSettings()
  };
