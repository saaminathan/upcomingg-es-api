function generateMappingSettings() {
    movieIndex = {
        settings: {
            index: {
            analysis: {
                filter: {
                permutations: {
                    max_shingle_size: "5",
                    min_shingle_size: "2",
                    type: "shingle"
                },
                autocomplete_filter: {
                    type: "edge_ngram",
                    min_gram: "1",
                    max_gram: "20"
                },
                delimiter: {
                    catenate_all: "true",
                    type: "word_delimiter",
                    catenate_numbers: "true",
                    preserve_original: "true",
                    catenate_words: "true"
                }
                },
                analyzer: {
                autocomplete: {
                    filter: [
                    "lowercase",
                    "autocomplete_filter",
                    "delimiter",
                    "permutations"
                    ],
                    type: "custom",
                    tokenizer: "standard"
                },
                my_english: {
                    type: "standard",
                    stopwords: "_english_"
                }
                },
                normalizer: {
                  my_normalizer: {
                    type: "custom",
                    char_filter: [],
                    filter: ["lowercase", "asciifolding"]
                  }
                }
            }
            }
        },
        mappings: {
            "movie":{
                "properties": {
                    "cast": {
                        "type": "keyword",
                        "store": true
                    },
                    "content_id": {
                        "type": "integer"
                    },
                    "coverImage": {
                        "type": "text",
                        "index": false
                    },
                    "crew": {
                        "type": "keyword",
                        "store": true
                    },
                    "duration": {
                      "type": "long"
                    },
                    "followCount": {
                      "type": "long"
                    },
                    "formattedLaunchDate": {
                      "type": "text",
                      "fields": {
                        "keyword": {
                          "type": "keyword",
                          "ignore_above": 256
                        }
                      }
                    },
                    "genre": {
                        "type": "keyword",
                        "store": true,
                        "normalizer": "my_normalizer"
                    },
                    "globalLaunch": {
                      "type": "boolean"
                    },
                    "images": {
                        "properties": {
                            "url": {"type": "text", "index": false},
                            "width": {"type": "integer", "index": false},
                            "height": {"type": "integer", "index": false}
                        }
                    },
                    "keywords": {
                        "type": "keyword",
                        "store": true
                    },
                    "languages": {
                        "type": "keyword",
                        "store": true
                    },
                    "launchCountries": {
                        "type": "keyword",
                        "store": true
                    },
                    "launchDate": {
                      "type": "date"
                    },
                    "launchDateEpoch": {
                      "type": "long"
                    },
                    "links": {
                        "properties": {
                            "url": {"type": "text", "index": false},
                            "name": {"type": "text", "index": false}
                        }
                    },
                    "movie_details_id": {
                      "type": "long"
                    },
                    "name": {
                        "type": "keyword",
                        "store": true
                    },
                    "objectID": {
                        "type": "text",
                        "index": false
                    },
                    "popularity": {
                      "type": "long"
                    },
                    "posterImage": {
                        "type": "text",
                        "index": false
                    },
                    "seo": {
                      "properties": {
                        "pageTitle": {"type": "text", "index": false},
                        "metaDescription": {"type": "text", "index": false},
                        "anchorText": {"type": "text", "index": false},
                        "robotsMetaTag": {"type": "text", "index": false}
                      }
                    },
                    "shortDesc": {
                      "type": "text",
                      "index": false
                    },
                    "slug": {
                        "type": "text",
                        "index": false
                    },
                    "songVideos": {
                      "properties": {
                        "youtube_id": {
                          "type": "text",
                          "fields": {
                            "keyword": {
                              "type": "keyword",
                              "ignore_above": 256
                            }
                          }
                        },
                        "youtube_title": {
                          "type": "text",
                          "fields": {
                            "keyword": {
                              "type": "keyword",
                              "ignore_above": 256
                            }
                          }
                        }
                      }
                    },
                    "trailerUrl": {
                      "type": "text",
                      "fields": {
                        "keyword": {
                          "type": "keyword",
                          "ignore_above": 256
                        }
                      }
                    },
                    "videoYoutubeId": {
                      "type": "text",
                      "fields": {
                        "keyword": {
                          "type": "keyword",
                          "ignore_above": 256
                        }
                      }
                    },
                    "videos": {
                      "properties": {
                        "youtube_id": {
                          "type": "text",
                          "fields": {
                            "keyword": {
                              "type": "keyword",
                              "ignore_above": 256
                            }
                          }
                        },
                        "youtube_title": {
                          "type": "text",
                          "fields": {
                            "keyword": {
                              "type": "keyword",
                              "ignore_above": 256
                            }
                          }
                        }
                      }
                    }
                }
            
            }
        }
    };
    return movieIndex;
  }
  
  module.exports = {
    MOVIE_INDEX_SETTINGS_MAPPING: generateMappingSettings()
  };